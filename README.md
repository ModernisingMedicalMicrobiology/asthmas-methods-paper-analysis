# asthmas methods paper analysis

Analysis scripts for "Identifying Bacterial Airways Infection in Stable Severe Asthma Using Oxford Nanopore Sequencing Technologies"

## ONT metagenomic results

Nanopore results were generated during the run using [CRuMPIT](https://gitlab.com/ModernisingMedicalMicrobiology/CRuMPIT), with the following commands.

```bash
data='/mnt/nanostore/ana/nick/hinks/rerun/data'
cdb='/mnt/nanostore/dbs/centrifuge/centrifuge_2019-09-24/centrifuge_2019-09-24'
wf='/mnt/nanostore/soft/CRuMPIT/workflows/PJI_unified_workflow.nf'
img='/mnt/nanostore/soft/images/crumpit-2019-10-14-7c864f81f302.img'


crumpit call \
    -s redo_${run} \
    -img $img \
    -f5s ${data}/${run}/multi/ \
    -bar  \
    -bc true \
    -bs 10 \
    -porechop guppy \
    -wf ${wf} \
    -cdb ${cdb} \
    -w

```

After all the data had been generated, the full results were compiled with the following commands for each run.

```bash
crumpit runstats -s redo_${run} -ip $ip -fp > data/CRuMPIT_output/${run}_filt_species.csv
crumpit runstats -s redo_${run} -ip $ip -sp  > data/CRuMPIT_output/${run}_all_species.csv
crumpit runstats -s redo_${run} -ip $ip -m |csvsort -c Barcode > data/CRuMPIT_output/${run}_map_species.csv
crumpit runstats -s redo_${run} -ip $ip > data/CRuMPIT_output/${run}_domain.csv
```

When all runs had been completed results were combined as such

```bash
csvstack data/CRuMPIT_output/*_all_species.csv > data/CRuMPIT_output/all_species.csv
```

## Illumina

Illumina data is not run through CRuMPIT which is designed for use only during ONT sequencing. For replication after the run, these commands can be used for ONT too.

The database/index used here was custom made on 2019-09-24.

```bash
cdb='/mnt/microbio/HOMES/nick/dbs/centrifuge/centrifuge_2019-09-13/centrifuge_2019-09-24'
centrifuge  --mm -x $cdb -q \
    -p 1 \
    -1 r1.fq \
    -2 r2.fq \
    -S fuge.txt \
    -k 1 \
    --report-file ${sample}.fuge.report.txt

centrifuge-kreport \
	-x $cdb \
	fuge.txt \
	> data/illumina/kreports/${sample}.fuge.kreport.txt
```

## plots

### Heatmap

The ONT heat map was produced using [groupBug](https://gitlab.com/ModernisingMedicalMicrobiology/groupBug) with these commands.

```bash
groupBug.py -k data/ONT/kreports/* \
    -suf *.txt \
    -meta meta.csv \
    -order order.csv \
    -sv figs/clustermap_ONT_species_zscore.pdf
```

The Illumina heat map was produced using [groupBug](https://gitlab.com/ModernisingMedicalMicrobiology/groupBug) with these commands.

```bash
groupBug.py -k data/illumina/kreports/* \
    -suf .fuge.kreport.txt \
    -meta illumina_meta.csv \
    -order order.csv \
    -sv figs/clustermap_miseq_species_zscore.pdf
```

### Coverage Breadth over depth

The coverage breadth over depth was created with the following script:
```bash
python3 bin/plot_breadth_depth.py
```

### Mapping consensus seqs

Consensus sequences and coverage stats were generated with the `rename.py` script:

The SNP distance matrix was generated with https://github.com/tseemann/snp-dists


