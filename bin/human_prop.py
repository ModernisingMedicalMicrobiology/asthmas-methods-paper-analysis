#!/usr/bin/env python
import pandas as pd
import glob
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

def getCompass(f):
    name=f.split('/')[-1].split('.')[0]
    df=pd.read_csv(f, sep='\t', names=['prop','culmulative taxon reads','taxon reads','taxon','taxid','species name'])
    df['sample name']=name
    df2=df[df['taxid']==9606]
    df2['human reads']=df2['culmulative taxon reads']
    df2=df2[['sample name','human reads']]
    df3=df[df['taxid']==2]
    df3['bacterial reads']=df3['culmulative taxon reads']
    df3=df3[['sample name','bacterial reads']]
    df=df3.merge(df2, on='sample name')
    return df


SampleOrder=[1054, 278, 133, 297, 288, 29, 214, 1049, 
        235, 306, 187, 1052, 294, 1055, 1053, 1041, 5, 
        275, 315, 1060, 279, 295, 1058, 
        'healthy control (HC149)', 'negative control']

SampleOrder=map(str,SampleOrder)

l=[]
for n,i in enumerate(SampleOrder):
    d={}
    d['PID']=str(i)
    d['order']=n
    l.append(d)
so=pd.DataFrame(l)



# ONT 
df0=pd.read_csv('data/CRuMPIT_output/BRC_TH_001-3.csv',sep='\t')
df1=pd.read_csv('data/CRuMPIT_output/original/BRC_TH_NP_domain.csv',sep='\t')
df2=pd.read_csv('data/CRuMPIT_output/original/BRC_TH_NP_2_domain.csv',sep='\t')
df1['human reads']=df1['hunam reads']
df2['human reads']=df2['hunam reads']


df=pd.concat([df0,df1,df2])
df['barcode']=df['barcode'].replace({'BC':'barcode'}, regex=True)
df['sample_ID']=df['sample_ID'].replace({'redo_':''}, regex=True)

# load meta data
meta=pd.read_csv('meta.csv')
meta['PID']=meta['newName'].map(str)
meta['Flow cell']=meta['Flow cell'].replace({'redo_':''}, regex=True)
meta=meta.merge(so,left_on='PID',right_on='PID',how='left')


# merge
df=df.merge(meta, left_on=['sample_ID','barcode'], 
        right_on=['Flow cell','Barcode'],
        how='left')
df=df.dropna(subset=['PID'])
df=df[df['barcode']!='nobarcode']
df['sample name']=df['oldName']
df['Total human reads removed']=df['human reads']
df['Total reads']=df['total reads']
df['Platform']='ONT'
df=df[['PID','sample name','Platform','Total human reads removed','human reads','bacterial reads','Total reads']]

# Illumina
df2=pd.read_csv('data/illumina/Illumina_human_reads_removed.csv')
meta=pd.read_csv('illumina_meta.csv')
meta['PID']=meta['newName'].map(str)
df2=df2.merge(meta,left_on=['sample name'], 
        right_on=['oldName'], 
        how='left')
df2=df2.dropna(subset=['PID'])
df3=pd.read_csv('data/seqkit/seqkit_data_meta.tsv',sep='\t')
df3=df3[df3['platform']=='Illumina']
df3['Total reads']=df3.groupby('sample')['num_seqs'].transform(sum)
df3=df3[['sample','Total reads']]
df3.drop_duplicates(inplace=True)
df2=df2.merge(df3, left_on='sample name', 
        right_on='sample',
        how='left')
df2['Total reads']=df2['Total human reads removed']+df2['Total reads']
df2['Platform']='Illumina'

## get bacterial from kreport
dfs=[]
for filename in glob.glob('data/illumina/kreports/*.txt'):
    kr=getCompass(filename)
    dfs.append(kr)
df3=pd.concat(dfs)

df2=df2.merge(df3,on='sample name',how='left')
df2['human reads']=df2['human reads']+df2['Total human reads removed']
df2=df2[['PID','sample name','Platform','Total human reads removed','human reads','bacterial reads','Total reads']]

## combine
df=pd.concat([df,df2])
print(df)
df.to_csv('human_bacterial_total_reads.csv',index=False)

## plots
df['Percentage of reads classed Human']=(df['human reads']/df['Total reads'])*100
ax1=sns.swarmplot(x='Platform', y='Percentage of reads classed Human', data=df)
ax1=sns.boxplot(x='Platform', y='Percentage of reads classed Human', data=df)
plt.savefig('figs/human_reads_percent.pdf')
