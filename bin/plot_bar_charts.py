import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
#sns.set_style('darkgrid')

SampleOrder=[1054, 278, 133, 297, 288, 29, 214, 1049, 
        235, 306, 187, 1052, 294, 1055, 1053, 1041, 5, 
        275, 315, 1060, 279, 295, 1058, 
        'healthy control (HC149)', 'negative control']

SampleOrder=map(str,SampleOrder)

l=[]
for n,i in enumerate(SampleOrder):
    d={}
    d['PID']=str(i)
    d['order']=n
    l.append(d)

so=pd.DataFrame(l)
print(so)

df=pd.read_csv('../gridion_2020_runs/domain.csv',sep='\t')
df1=pd.read_csv('../ONT/BRC_TH_NP_domain.csv',sep='\t')
df2=pd.read_csv('../ONT/BRC_TH_NP_2_domain.csv',sep='\t')
dfs=[df,df1,df2]
df=pd.concat(dfs)

# load meta data
meta=pd.read_csv('meta.csv')
meta['PID']=meta['PID'].map(str)
meta=meta.merge(so,left_on='PID',right_on='PID',how='left')
print(meta)

# merge
df=df.merge(meta, left_on=['sample_ID','barcode'], 
        right_on=['Flow cell','Barcode'],
        how='left')
df=df.dropna(subset=['Sample'])
df=df[df['barcode']!='nobarcode']
print(df)

df['Bacterial']=df['bacterial bases']/1000000000
df['Human']=df['human bases']/1000000000
df['Viral']=df['viral bases']/1000000000
df['Unclassified']=(df['total bases'] - (df['bacterial bases']+df['human bases']+df['viral bases']))/1000000000

df['Bacterial prop']=df['bacterial bases']/df['total bases']
df['Human prop']=df['human bases']/df['total bases']
df['Viral prop']=df['viral bases']/df['total bases']
df['Unknown prop']=(df['total bases'] - (df['bacterial bases']+df['human bases']+df['viral bases']))/df['total bases']


# drop
df['Sample name']=df['PID']#df['sample_ID']+' '+df['barcode']
df2=df.dropna(subset=['order'])
#sort
df2.sort_values(by='order',ascending=False, inplace=True)
cmap=[
        (234,234,234),
        (192,192,192),
        (121,121,121),
        (32,32,32)
        ]

cmap2=[]
for i in cmap:
    tu=[]
    for t in i:
        tu.append(t/255.0)
    cmap2.append(tuple(tu))

ax=df2[['Sample name','Viral','Bacterial','Human','Unclassified']].plot(kind='barh', stacked=True, x='Sample name',color=cmap2)
#ax=df[['Sample name','Bacterial prop','Human prop','Viral prop','Unknown prop']].plot(kind='barh', stacked=True, x='Sample name')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10),
                  ncol=4, fancybox=True, shadow=False)
plt.tight_layout()
plt.xlabel("Gigabases")
#plt.xlabel("Proportion of bases")
plt.savefig('stacked_gigabases_domain.pdf')
plt.savefig('stacked_gigabases_domain.svg')
plt.savefig('stacked_gigabases_domain.png')
plt.show()
#plt.savefig('total_numbers.pdf')

