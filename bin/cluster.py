from argparse import ArgumentParser, SUPPRESS
import pandas as pd
from ete3 import NCBITaxa
import matplotlib
# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import seaborn as sns; sns.set(color_codes=True)
#ncbi = NCBITaxa(dbfile='/Users/nick/.etetoolkit_odin/taxa.sqlite')
ncbi = NCBITaxa()

SampleOrder=[1054, 278, 133, 297, 288, 29, 214, 1049,
        235, 306, 187, 1052, 294, 1055, 1053, 1041, 5,
        275, 315, 1060, 279, 295, 1058,
        'healthy control (HC149)', 'negative control']

SampleOrder=list(map(str,SampleOrder))
l=[]
for n,i in enumerate(SampleOrder):
    d={}
    d['PID']=str(i)
    d['order']=n
    l.append(d)
soL=['Taxid']
sol=soL.extend(SampleOrder)
print(soL)
so=pd.DataFrame(l)

def getData(csv,collapse=False):
    if type(csv)==list:
        dfs=[]
        for c in csv:
            df=pd.read_csv(c)
            dfs.append(df)
        df=pd.concat(dfs)
    else:
        df=pd.read_csv(csv)
    #df['sampleName']=df['Run_name']+'_'+df['Barcode']
    if collapse==True:
        df=df.groupby(['Run_name','Taxid']).sum()
        df.reset_index(inplace=True)
    else:
        df['Run_name']=df['Run_name']+'_'+df['Barcode']
        df=df[['Run_name','Taxid','Bases','Reads']]
        df=df.groupby(['Run_name','Taxid']).sum()
        df.reset_index(inplace=True)
    
    # add names # add names # add names 
    ti=ncbi.get_name_translator(df.Taxid)
    def getTax(r):
        try:
            t=ti[r][0]
        except:
            t=None
       # t=ti[r][0]
        return t

    df['t']=df['Taxid'].map(getTax)
    df.dropna(subset=['t'],inplace=True)
    b=ncbi.get_descendant_taxa(2,intermediate_nodes=True)

    # filter for bacteria and reads 10 reads
    df=df[df.t.isin(b)]
    df=df[df.Reads > 1]
    return df


def rename(df,meta):
    # rename from metadata sheet
    meta=pd.read_csv(meta)
    meta['Run_name']=meta['Flow cell']+'_'+meta['Barcode']
    df=df.merge(meta,on='Run_name',how='left')
    df['Run_name']=df['PID']
    df=df[['Run_name','Taxid','Bases','Reads']]
    df=df.dropna(subset=['Run_name'])
    df=df[df['Run_name']!='0']
    return df 

def manipulate(df,cluster=True):
    # reorder if desired
    if cluster==False:
        df=df.merge(so,left_on='Run_name',right_on='PID',how='left')
        df.sort_values(by='order',ascending=False, inplace=True)
        print(df)
        df=df[['Run_name','Taxid','Bases','Reads']]

    # manipulate to pivot table
#    df = df.pivot(index='Taxid',columns='Run_name',values='Bases')
    df = df.pivot(index='Taxid',columns='Run_name',values='Reads')
    
    df['total']=df.sum(axis=1,numeric_only=True)
    df=df.nlargest(30, 'total')
    df=df.drop('total',axis=1)
    #df.reset_index(inplace=True)
    df.to_csv('matric.csv')
#    df.fillna(value=0.0000001,inplace=True)
    df.fillna(value=0,inplace=True)
#    df=df.reset_index()
    print(df)
    df=df[SampleOrder]
    print(df)
    return df

def plot(df,name,cluster):
    ax = sns.clustermap(df,
                    cmap="Blues",
                    metric='euclidean',
                    method='centroid',
                    col_cluster=cluster,
#                    vmax=5000,
                    z_score=1,
                    xticklabels=True,
                    figsize=(12, 12))

    plt.gcf().subplots_adjust(bottom=0.30,right=0.75,left=0,top=0.98)
    plt.savefig('{0}'.format(name))


def run(opts):
    df=getData(opts.csv,collapse=opts.collapse)
    if opts.meta != None:
        df=rename(df,opts.meta)
    df=manipulate(df,cluster=opts.cluster)
    plot(df,opts.name,opts.cluster)


if __name__ == '__main__':
    parser = ArgumentParser(description='cluster map from crumpit csv')
    parser.add_argument('-csv', '--csv', required=True,nargs='+',
                        help='csv file')
    parser.add_argument('-m', '--meta', required=False, default=None,
                        help='meta csv file')
    parser.add_argument('-n', '--name', required=True,
                        help='name for output')
    parser.add_argument('-c', '--collapse', required=False,action='store_true',
                        help='collapse barcodes into run and sum all')
    parser.add_argument('-l', '--cluster', required=False,action='store_false',
                        help='Cluster columns, default=True')
    opts, unknown_args = parser.parse_known_args()

    run(opts)


