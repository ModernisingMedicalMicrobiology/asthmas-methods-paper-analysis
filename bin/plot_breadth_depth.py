#!/usr/bin/env python3
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('darkgrid')

df=pd.read_csv('data/illumina/mapped_combined.csv')
df['Average depth of genome covered (x fold)']=df['depth']
df['Breadth of genome covered (%)']=df['breadth']

df2=pd.read_csv('data/ONT/map_species.csv')
def runBar(r):
    return '{0}_{1}'.format(r['sample'],r['barcode'])
df2['runBar']=df2.apply(runBar,axis=1)
df2=df2[df2['chrom']=='NC_000907_1']
df2=df2[['runBar','covBread_1x_percent','avCov']]
df2.rename(columns={'runBar':'sample',
    'covBread_1x_percent':'Breadth of genome covered (%)',
    'avCov':'Average depth of genome covered (x fold)'},inplace=True)
df2['platform']='ONT'
df=pd.concat([df,df2])

df['Sequencing platform']=df['platform']
p=sns.scatterplot(x='Average depth of genome covered (x fold)',
    y='Breadth of genome covered (%)',
    hue='Sequencing platform',data=df)
plt.title('Coverage breadth over coverage depth for H. influenzae')
plt.savefig('figs/breadthVSdepth_update20211001.pdf')
plt.savefig('figs/breadthVSdepth_update20211001.eps')
plt.savefig('figs/breadthVSdepth_update20211001.png',dpi=300)

#df.to_csv('all_map_data.csv',index=False)
