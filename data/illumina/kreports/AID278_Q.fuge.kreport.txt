  9.62	6342	6342	U	0	unclassified
 90.38	59582	25	-	1	root
 90.33	59548	0	-	131567	  cellular organisms
 47.14	31075	3	D	2759	    Eukaryota
 47.13	31070	0	-	33154	      Opisthokonta
 47.12	31066	0	K	33208	        Metazoa
 47.12	31066	0	-	6072	          Eumetazoa
 47.12	31066	0	-	33213	            Bilateria
 47.12	31066	0	-	33511	              Deuterostomia
 47.12	31066	0	P	7711	                Chordata
 47.12	31066	0	-	89593	                  Craniata
 47.12	31066	0	-	7742	                    Vertebrata
 47.12	31066	0	-	7776	                      Gnathostomata
 47.12	31066	0	-	117570	                        Teleostomi
 47.12	31066	0	-	117571	                          Euteleostomi
 47.12	31066	0	-	8287	                            Sarcopterygii
 47.12	31066	0	-	1338369	                              Dipnotetrapodomorpha
 47.12	31066	0	-	32523	                                Tetrapoda
 47.12	31066	0	-	32524	                                  Amniota
 47.12	31066	0	C	40674	                                    Mammalia
 47.12	31066	0	-	32525	                                      Theria
 47.12	31066	0	-	9347	                                        Eutheria
 47.12	31066	0	-	1437010	                                          Boreoeutheria
 47.12	31066	0	-	314146	                                            Euarchontoglires
 47.12	31066	0	O	9443	                                              Primates
 47.12	31066	0	-	376913	                                                Haplorrhini
 47.12	31066	0	-	314293	                                                  Simiiformes
 47.12	31066	0	-	9526	                                                    Catarrhini
 47.12	31066	0	-	314295	                                                      Hominoidea
 47.12	31066	0	F	9604	                                                        Hominidae
 47.12	31066	0	-	207598	                                                          Homininae
 47.12	31066	0	G	9605	                                                            Homo
 47.12	31066	31066	S	9606	                                                              Homo sapiens
  0.01	4	0	K	4751	        Fungi
  0.00	3	0	-	451864	          Dikarya
  0.00	2	0	P	5204	            Basidiomycota
  0.00	1	0	-	5302	              Agaricomycotina
  0.00	1	0	C	155616	                Tremellomycetes
  0.00	1	0	O	5234	                  Tremellales
  0.00	1	0	F	1884633	                    Cryptococcaceae
  0.00	1	0	G	5206	                      Cryptococcus
  0.00	1	0	S	1897064	                        Cryptococcus neoformans species complex
  0.00	1	0	S	5207	                          Cryptococcus neoformans
  0.00	1	0	-	40410	                            Cryptococcus neoformans var. neoformans
  0.00	1	1	-	214684	                              Cryptococcus neoformans var. neoformans JEC21
  0.00	1	0	-	452284	              Ustilaginomycotina
  0.00	1	0	C	5257	                Ustilaginomycetes
  0.00	1	0	O	5267	                  Ustilaginales
  0.00	1	0	F	5268	                    Ustilaginaceae
  0.00	1	0	G	5269	                      Ustilago
  0.00	1	0	S	5270	                        Ustilago maydis
  0.00	1	1	-	237631	                          Ustilago maydis 521
  0.00	1	1	P	4890	            Ascomycota
  0.00	1	0	-	112252	          Fungi incertae sedis
  0.00	1	0	P	6029	            Microsporidia
  0.00	1	0	-	6032	              Apansporoblastina
  0.00	1	0	F	36734	                Unikaryonidae
  0.00	1	0	G	6033	                  Encephalitozoon
  0.00	1	0	S	571949	                    Encephalitozoon romaleae
  0.00	1	1	-	1178016	                      Encephalitozoon romaleae SJ-2008
  0.00	1	0	-	33630	      Alveolata
  0.00	1	0	P	5794	        Apicomplexa
  0.00	1	0	C	422676	          Aconoidasida
  0.00	1	0	O	5819	            Haemosporida
  0.00	1	0	F	1639119	              Plasmodiidae
  0.00	1	0	G	5820	                Plasmodium
  0.00	1	0	-	418107	                  Plasmodium (Laverania)
  0.00	1	0	S	5833	                    Plasmodium falciparum
  0.00	1	1	-	36329	                      Plasmodium falciparum 3D7
  0.00	1	0	-	33682	      Euglenozoa
  0.00	1	0	O	5653	        Kinetoplastida
  0.00	1	0	F	5654	          Trypanosomatidae
  0.00	1	0	-	1286322	            Leishmaniinae
  0.00	1	0	G	5658	              Leishmania
  0.00	1	0	-	38568	                Leishmania
  0.00	1	0	S	38581	                  Leishmania major species complex
  0.00	1	0	S	5664	                    Leishmania major
  0.00	1	1	-	347515	                      Leishmania major strain Friedlin
 43.19	28472	17	D	2	    Bacteria
 42.81	28223	28	P	1224	      Proteobacteria
 42.73	28167	90	C	1236	        Gammaproteobacteria
 42.33	27906	0	O	135625	          Pasteurellales
 42.33	27906	277	F	712	            Pasteurellaceae
 41.87	27605	4151	G	724	              Haemophilus
 35.44	23361	22911	S	727	                Haemophilus influenzae
  0.10	68	68	-	281310	                  Haemophilus influenzae 86-028NP
  0.10	65	65	-	262728	                  Haemophilus influenzae R2866
  0.09	60	60	-	1334187	                  Haemophilus influenzae KR494
  0.08	51	51	-	262727	                  Haemophilus influenzae R2846
  0.08	51	51	-	862964	                  Haemophilus influenzae 10810
  0.07	48	48	-	1232659	                  Haemophilus influenzae 2019
  0.04	28	28	-	71421	                  Haemophilus influenzae Rd KW20
  0.04	25	25	-	1295140	                  Haemophilus influenzae CGSHiCZ412602
  0.03	20	20	-	374930	                  Haemophilus influenzae PittEE
  0.03	20	20	-	866630	                  Haemophilus influenzae F3031
  0.02	14	14	-	935897	                  Haemophilus influenzae F3047
  0.05	31	31	S	726	                Haemophilus haemolyticus
  0.04	29	15	S	729	                Haemophilus parainfluenzae
  0.02	14	14	-	862965	                  Haemophilus parainfluenzae T3T1
  0.03	19	19	S	730	                [Haemophilus] ducreyi
  0.02	10	10	S	197575	                Haemophilus aegyptius
  0.01	4	4	S	712310	                Haemophilus sp. oral taxon 036
  0.02	11	0	G	745	              Pasteurella
  0.02	11	8	S	747	                Pasteurella multocida
  0.00	2	2	-	1304890	                  Pasteurella multocida OH1905
  0.00	1	0	-	44283	                  Pasteurella multocida subsp. multocida
  0.00	1	1	-	272843	                    Pasteurella multocida subsp. multocida str. Pm70
  0.01	5	0	G	713	              Actinobacillus
  0.01	4	0	S	715	                Actinobacillus pleuropneumoniae
  0.01	4	4	-	754345	                  Actinobacillus pleuropneumoniae serovar 8
  0.00	1	1	S	51161	                Actinobacillus delphinicola
  0.01	4	0	G	416916	              Aggregatibacter
  0.00	3	3	S	732	                Aggregatibacter aphrophilus
  0.00	1	0	S	739	                Aggregatibacter segnis
  0.00	1	1	-	888057	                  Aggregatibacter segnis ATCC 33393
  0.00	2	0	G	75984	              Mannheimia
  0.00	1	1	S	75985	                Mannheimia haemolytica
  0.00	1	1	S	1432056	                Mannheimia sp. USDA-ARS-USMARC-1261
  0.00	1	0	G	214906	              Histophilus
  0.00	1	1	S	731	                Histophilus somni
  0.00	1	0	G	292486	              Avibacterium
  0.00	1	1	S	728	                Avibacterium paragallinarum
  0.15	97	0	O	72274	          Pseudomonadales
  0.14	95	0	F	468	            Moraxellaceae
  0.14	93	0	G	475	              Moraxella
  0.14	90	90	S	480	                Moraxella catarrhalis
  0.00	1	1	S	476	                Moraxella bovis
  0.00	1	1	S	34061	                Moraxella cuniculi
  0.00	1	1	S	34062	                Moraxella osloensis
  0.00	2	0	G	469	              Acinetobacter
  0.00	1	1	S	29430	                Acinetobacter haemolyticus
  0.00	1	0	S	909768	                Acinetobacter calcoaceticus/baumannii complex
  0.00	1	1	S	470	                  Acinetobacter baumannii
  0.00	2	0	F	135621	            Pseudomonadaceae
  0.00	1	0	G	286	              Pseudomonas
  0.00	1	0	S	136843	                Pseudomonas fluorescens group
  0.00	1	1	S	47883	                  Pseudomonas synxantha
  0.00	1	0	G	1849530	              Oblitimonas
  0.00	1	1	S	1697053	                Oblitimonas alkaliphila
  0.08	52	4	O	91347	          Enterobacterales
  0.05	33	3	F	543	            Enterobacteriaceae
  0.02	13	0	G	561	              Escherichia
  0.02	13	11	S	562	                Escherichia coli
  0.00	1	0	-	83334	                  Escherichia coli O157:H7
  0.00	1	1	-	1328859	                    Escherichia coli O157:H7 str. SS17
  0.00	1	1	-	910348	                  Escherichia coli P12b
  0.01	8	0	G	590	              Salmonella
  0.01	8	1	S	28901	                Salmonella enterica
  0.01	7	2	-	59201	                  Salmonella enterica subsp. enterica
  0.00	1	1	-	611	                    Salmonella enterica subsp. enterica serovar Heidelberg
  0.00	1	0	-	58712	                    Salmonella enterica subsp. enterica serovar Anatum
  0.00	1	1	-	1454584	                      Salmonella enterica subsp. enterica serovar Anatum str. USDA-ARS-USMARC-1727
  0.00	1	1	-	90105	                    Salmonella enterica subsp. enterica serovar Saintpaul
  0.00	1	1	-	108619	                    Salmonella enterica subsp. enterica serovar Newport
  0.00	1	1	-	260678	                    Salmonella enterica subsp. enterica serovar Goldcoast
  0.01	6	0	G	570	              Klebsiella
  0.01	4	1	S	573	                Klebsiella pneumoniae
  0.00	2	2	-	72407	                  Klebsiella pneumoniae subsp. pneumoniae
  0.00	1	1	-	1049565	                  Klebsiella pneumoniae KCTC 2242
  0.00	2	2	S	1134687	                Klebsiella michiganensis
  0.00	2	0	G	547	              Enterobacter
  0.00	2	0	S	354276	                Enterobacter cloacae complex
  0.00	2	0	S	550	                  Enterobacter cloacae
  0.00	2	0	-	336306	                    Enterobacter cloacae subsp. cloacae
  0.00	2	2	-	716541	                      Enterobacter cloacae subsp. cloacae ATCC 13047
  0.00	1	0	G	579	              Kluyvera
  0.00	1	1	S	61648	                Kluyvera intermedia
  0.01	6	0	F	1903410	            Pectobacteriaceae
  0.00	2	1	G	84565	              Sodalis
  0.00	1	1	S	1239307	                Sodalis praecaptivus
  0.00	2	0	G	122277	              Pectobacterium
  0.00	2	1	S	554	                Pectobacterium carotovorum
  0.00	1	0	-	555	                  Pectobacterium carotovorum subsp. carotovorum
  0.00	1	1	-	1218933	                    Pectobacterium carotovorum subsp. carotovorum PCC21
  0.00	2	1	G	204037	              Dickeya
  0.00	1	0	S	204042	                Dickeya zeae
  0.00	1	1	-	590409	                  Dickeya zeae Ech586
  0.00	3	0	F	1903409	            Erwiniaceae
  0.00	1	0	G	551	              Erwinia
  0.00	1	1	S	1922217	                Candidatus Erwinia haradaeae
  0.00	1	0	G	53335	              Pantoea
  0.00	1	1	S	1235990	                Candidatus Pantoea carbekii
  0.00	1	0	G	82986	              Tatumella
  0.00	1	1	S	82987	                Tatumella ptyseos
  0.00	2	0	F	1903411	            Yersiniaceae
  0.00	1	0	G	629	              Yersinia
  0.00	1	0	S	1649845	                Yersinia pseudotuberculosis complex
  0.00	1	0	S	632	                  Yersinia pestis
  0.00	1	1	-	1345707	                    Yersinia pestis 3770
  0.00	1	0	G	1927833	              Candidatus Fukatsuia
  0.00	1	1	S	1878942	                Candidatus Fukatsuia symbiotica
  0.00	2	0	F	1903412	            Hafniaceae
  0.00	2	1	G	635	              Edwardsiella
  0.00	1	0	S	67780	                Edwardsiella ictaluri
  0.00	1	1	-	634503	                  Edwardsiella ictaluri 93-146
  0.00	1	0	-	451511	            unclassified Enterobacterales
  0.00	1	0	G	702	              Plesiomonas
  0.00	1	1	S	703	                Plesiomonas shigelloides
  0.00	1	0	F	1903414	            Morganellaceae
  0.00	1	0	G	583	              Proteus
  0.00	1	1	S	585	                Proteus vulgaris
  0.01	5	1	O	135622	          Alteromonadales
  0.00	2	1	F	72275	            Alteromonadaceae
  0.00	1	0	G	288793	              Salinimonas
  0.00	1	1	S	2303538	                Salinimonas sediminis
  0.00	1	0	F	267888	            Pseudoalteromonadaceae
  0.00	1	1	G	53246	              Pseudoalteromonas
  0.00	1	0	F	267894	            Psychromonadaceae
  0.00	1	0	G	67572	              Psychromonas
  0.00	1	1	S	314282	                Psychromonas sp. CNPT3
  0.01	4	0	O	135613	          Chromatiales
  0.00	2	0	F	1046	            Chromatiaceae
  0.00	1	0	G	1227	              Nitrosococcus
  0.00	1	0	S	1229	                Nitrosococcus oceani
  0.00	1	1	-	323261	                  Nitrosococcus oceani ATCC 19707
  0.00	1	0	G	13724	              Thiocystis
  0.00	1	0	S	73141	                Thiocystis violascens
  0.00	1	1	-	765911	                  Thiocystis violascens DSM 198
  0.00	2	0	F	72276	            Ectothiorhodospiraceae
  0.00	1	1	G	85108	              Halorhodospira
  0.00	1	0	G	106633	              Thioalkalivibrio
  0.00	1	0	S	1033854	                Thioalkalivibrio sulfidiphilus
  0.00	1	1	-	396588	                  Thioalkalivibrio sulfidiphilus HL-EbGr7
  0.00	2	0	O	72273	          Thiotrichales
  0.00	1	0	F	34064	            Francisellaceae
  0.00	1	0	G	262	              Francisella
  0.00	1	1	S	573570	                Francisella sp. TX077310
  0.00	1	0	F	135616	            Piscirickettsiaceae
  0.00	1	0	G	1237	              Piscirickettsia
  0.00	1	1	S	1238	                Piscirickettsia salmonis
  0.00	2	0	O	118969	          Legionellales
  0.00	2	0	F	444	            Legionellaceae
  0.00	2	0	G	445	              Legionella
  0.00	1	1	S	446	                Legionella pneumophila
  0.00	1	0	S	29423	                Legionella oakridgensis
  0.00	1	1	-	1268635	                  Legionella oakridgensis ATCC 33761 = DSM 21215
  0.00	2	0	O	135623	          Vibrionales
  0.00	2	0	F	641	            Vibrionaceae
  0.00	1	0	G	662	              Vibrio
  0.00	1	1	S	29494	                Vibrio furnissii
  0.00	1	0	G	511678	              Aliivibrio
  0.00	1	1	S	40269	                Aliivibrio salmonicida
  0.00	2	0	O	135624	          Aeromonadales
  0.00	2	0	F	84642	            Aeromonadaceae
  0.00	2	0	G	642	              Aeromonas
  0.00	1	1	S	644	                Aeromonas hydrophila
  0.00	1	1	S	654	                Aeromonas veronii
  0.00	2	0	O	1692040	          Acidiferrobacterales
  0.00	2	0	F	1692041	            Acidiferrobacteraceae
  0.00	1	0	G	1692042	              Sulfurifustis
  0.00	1	1	S	1675686	                Sulfurifustis variabilis
  0.00	1	0	G	1744881	              Sulfuricaulis
  0.00	1	1	S	1620215	                Sulfuricaulis limicola
  0.00	1	0	O	135614	          Xanthomonadales
  0.00	1	0	F	32033	            Xanthomonadaceae
  0.00	1	1	G	40323	              Stenotrophomonas
  0.00	1	0	O	135619	          Oceanospirillales
  0.00	1	0	F	28256	            Halomonadaceae
  0.00	1	0	G	2745	              Halomonas
  0.00	1	1	S	2136172	                Halomonas sp. SF2003
  0.00	1	0	O	1775403	          Nevskiales
  0.00	1	0	F	568386	            Sinobacteraceae
  0.00	1	0	G	413435	              Solimonas
  0.00	1	1	S	2303331	                Solimonas sp. K1W22B-7
  0.02	12	1	C	28216	        Betaproteobacteria
  0.01	8	1	O	80840	          Burkholderiales
  0.00	3	1	F	80864	            Comamonadaceae
  0.00	2	1	G	12916	              Acidovorax
  0.00	1	1	S	232721	                Acidovorax sp. JS42
  0.00	2	0	F	119060	            Burkholderiaceae
  0.00	1	0	G	48736	              Ralstonia
  0.00	1	0	S	329	                Ralstonia pickettii
  0.00	1	1	-	402626	                  Ralstonia pickettii 12J
  0.00	1	0	G	106589	              Cupriavidus
  0.00	1	0	S	106590	                Cupriavidus necator
  0.00	1	1	-	1042878	                  Cupriavidus necator N-1
  0.00	1	0	F	506	            Alcaligenaceae
  0.00	1	0	-	83496	              unclassified Alcaligenaceae
  0.00	1	1	S	2593958	                Alcaligenaceae bacterium SJ-26
  0.00	1	0	F	75682	            Oxalobacteraceae
  0.00	1	0	G	846	              Oxalobacter
  0.00	1	1	S	847	                Oxalobacter formigenes
  0.00	3	0	O	206351	          Neisseriales
  0.00	3	0	F	481	            Neisseriaceae
  0.00	3	1	G	482	              Neisseria
  0.00	1	1	S	485	                Neisseria gonorrhoeae
  0.00	1	1	S	28449	                Neisseria subflava
  0.01	8	0	-	68525	        delta/epsilon subdivisions
  0.01	4	1	C	28221	          Deltaproteobacteria
  0.00	2	0	O	29	            Myxococcales
  0.00	1	0	-	80811	              Cystobacterineae
  0.00	1	0	F	31	                Myxococcaceae
  0.00	1	0	G	32	                  Myxococcus
  0.00	1	0	S	33	                    Myxococcus fulvus
  0.00	1	1	-	1334629	                      Myxococcus fulvus 124B02
  0.00	1	0	-	80812	              Sorangiineae
  0.00	1	0	F	49	                Polyangiaceae
  0.00	1	0	G	39643	                  Sorangium
  0.00	1	0	S	56	                    Sorangium cellulosum
  0.00	1	1	-	1254432	                      Sorangium cellulosum So0157-2
  0.00	1	0	O	213462	            Syntrophobacterales
  0.00	1	0	F	213468	              Syntrophaceae
  0.00	1	0	G	43773	                Syntrophus
  0.00	1	0	S	316277	                  Syntrophus aciditrophicus
  0.00	1	1	-	56780	                    Syntrophus aciditrophicus SB
  0.01	4	0	C	29547	          Epsilonproteobacteria
  0.01	4	0	O	213849	            Campylobacterales
  0.01	4	1	F	72294	              Campylobacteraceae
  0.00	2	0	-	2321108	                Arcobacter group
  0.00	2	0	G	28196	                  Arcobacter
  0.00	1	0	S	28199	                    Arcobacter nitrofigilis
  0.00	1	1	-	572480	                      Arcobacter nitrofigilis DSM 7299
  0.00	1	1	S	197482	                    Arcobacter halophilus
  0.00	1	0	G	194	                Campylobacter
  0.00	1	1	S	199	                  Campylobacter concisus
  0.01	6	0	C	28211	        Alphaproteobacteria
  0.01	4	1	O	356	          Rhizobiales
  0.00	2	0	F	41294	            Bradyrhizobiaceae
  0.00	1	0	G	374	              Bradyrhizobium
  0.00	1	1	S	2057741	                Bradyrhizobium sp. SK17
  0.00	1	0	G	1649510	              Variibacter
  0.00	1	1	S	1333996	                Variibacter gotjawalensis
  0.00	1	0	F	335928	            Xanthobacteraceae
  0.00	1	0	G	6	              Azorhizobium
  0.00	1	0	S	7	                Azorhizobium caulinodans
  0.00	1	1	-	438753	                  Azorhizobium caulinodans ORS 571
  0.00	1	0	O	54526	          Pelagibacterales
  0.00	1	0	G	2045213	            Candidatus Fonsibacter
  0.00	1	1	S	1925548	              Candidatus Fonsibacter ubiquis
  0.00	1	0	O	204457	          Sphingomonadales
  0.00	1	0	F	41297	            Sphingomonadaceae
  0.00	1	0	G	165695	              Sphingobium
  0.00	1	1	S	2082188	                Sphingobium sp. YG1
  0.00	1	0	C	580370	        Zetaproteobacteria
  0.00	1	0	O	580371	          Mariprofundales
  0.00	1	0	F	580372	            Mariprofundaceae
  0.00	1	0	G	377315	              Mariprofundus
  0.00	1	1	S	1921086	                Mariprofundus aestuarium
  0.00	1	1	C	1553900	        Oligoflexia
  0.28	185	0	-	1783272	      Terrabacteria group
  0.21	141	0	P	1239	        Firmicutes
  0.14	94	0	C	91061	          Bacilli
  0.13	84	0	O	186826	            Lactobacillales
  0.13	83	0	F	1300	              Streptococcaceae
  0.13	83	11	G	1301	                Streptococcus
  0.04	27	0	S	257758	                  Streptococcus pseudopneumoniae
  0.04	27	27	-	1054460	                    Streptococcus pseudopneumoniae IS7493
  0.02	16	13	S	1313	                  Streptococcus pneumoniae
  0.00	1	1	-	487214	                    Streptococcus pneumoniae Hungary19A-6
  0.00	1	1	-	488221	                    Streptococcus pneumoniae 70585
  0.00	1	1	-	869309	                    Streptococcus pneumoniae SPNA45
  0.02	11	3	S	1318	                  Streptococcus parasanguinis
  0.01	6	6	-	760570	                    Streptococcus parasanguinis ATCC 15912
  0.00	2	2	-	1114965	                    Streptococcus parasanguinis FW213
  0.02	11	9	S	28037	                  Streptococcus mitis
  0.00	2	2	-	246201	                    Streptococcus mitis NCTC 12261
  0.00	2	2	S	1303	                  Streptococcus oralis
  0.00	2	1	S	1304	                  Streptococcus salivarius
  0.00	1	1	-	1048332	                    Streptococcus salivarius CCHSS3
  0.00	1	0	S	1307	                  Streptococcus suis
  0.00	1	1	-	1004952	                    Streptococcus suis D12
  0.00	1	0	S	45634	                  Streptococcus cristatus
  0.00	1	1	-	889201	                    Streptococcus cristatus ATCC 51100
  0.00	1	0	S	671232	                  Streptococcus anginosus group
  0.00	1	1	S	76860	                    Streptococcus constellatus
  0.00	1	0	F	186828	              Carnobacteriaceae
  0.00	1	0	G	191769	                Marinilactibacillus
  0.00	1	1	S	1911586	                  Marinilactibacillus sp. 15R
  0.02	10	0	O	1385	            Bacillales
  0.00	3	0	F	186817	              Bacillaceae
  0.00	2	0	G	1386	                Bacillus
  0.00	1	0	S	1398	                  Bacillus coagulans
  0.00	1	1	-	941639	                    Bacillus coagulans 2-6
  0.00	1	1	S	1178537	                  Bacillus xiamenensis
  0.00	1	0	G	1055323	                Aeribacillus
  0.00	1	1	S	33936	                  Aeribacillus pallidus
  0.00	3	0	F	186822	              Paenibacillaceae
  0.00	2	0	G	44249	                Paenibacillus
  0.00	1	1	S	189425	                  Paenibacillus graminis
  0.00	1	1	S	1712516	                  Paenibacillus baekrokdamisoli
  0.00	1	0	G	329857	                Cohnella
  0.00	1	1	S	2507935	                  Cohnella sp. HS21
  0.00	1	0	F	90964	              Staphylococcaceae
  0.00	1	0	G	1279	                Staphylococcus
  0.00	1	1	S	985002	                  Staphylococcus argenteus
  0.00	1	0	F	186818	              Planococcaceae
  0.00	1	0	G	1372	                Planococcus
  0.00	1	1	S	192421	                  Planococcus maritimus
  0.00	1	0	F	186820	              Listeriaceae
  0.00	1	0	G	1637	                Listeria
  0.00	1	1	S	1639	                  Listeria monocytogenes
  0.00	1	0	-	539002	              Bacillales incertae sedis
  0.00	1	0	-	539738	                Bacillales Family XI. Incertae Sedis
  0.00	1	1	G	1378	                  Gemella
  0.05	35	0	C	909932	          Negativicutes
  0.05	35	0	O	1843489	            Veillonellales
  0.05	35	0	F	31977	              Veillonellaceae
  0.05	33	0	G	29465	                Veillonella
  0.04	25	25	S	29466	                  Veillonella parvula
  0.01	8	8	S	39778	                  Veillonella dispar
  0.00	2	0	G	39948	                Dialister
  0.00	2	2	S	39950	                  Dialister pneumosintes
  0.02	12	0	C	186801	          Clostridia
  0.02	10	0	O	186802	            Clostridiales
  0.01	5	1	F	31979	              Clostridiaceae
  0.01	4	1	G	1485	                Clostridium
  0.00	2	0	S	1542	                  Clostridium novyi
  0.00	2	2	-	386415	                    Clostridium novyi NT
  0.00	1	0	S	1493	                  Clostridium cellulovorans
  0.00	1	1	-	573061	                    Clostridium cellulovorans 743B
  0.00	1	0	F	186804	              Peptostreptococcaceae
  0.00	1	0	G	1870884	                Clostridioides
  0.00	1	1	S	1496	                  Clostridioides difficile
  0.00	1	0	F	186806	              Eubacteriaceae
  0.00	1	0	G	33951	                Acetobacterium
  0.00	1	0	S	33952	                  Acetobacterium woodii
  0.00	1	1	-	931626	                    Acetobacterium woodii DSM 1030
  0.00	1	0	F	186807	              Peptococcaceae
  0.00	1	0	G	51514	                Dehalobacterium
  0.00	1	1	S	51515	                  Dehalobacterium formicoaceticum
  0.00	1	0	-	538999	              Clostridiales incertae sedis
  0.00	1	0	F	543314	                Clostridiales Family XIII. Incertae Sedis
  0.00	1	0	G	86331	                  Mogibacterium
  0.00	1	1	S	114527	                    Mogibacterium diversum
  0.00	1	0	F	2603322	              Vallitaleaceae
  0.00	1	0	G	2603323	                Petrocella
  0.00	1	1	S	2173034	                  Petrocella atlantisensis
  0.00	2	0	O	68295	            Thermoanaerobacterales
  0.00	1	0	F	186814	              Thermoanaerobacteraceae
  0.00	1	0	G	140458	                Thermacetogenium
  0.00	1	0	S	85874	                  Thermacetogenium phaeum
  0.00	1	1	-	1089553	                    Thermacetogenium phaeum DSM 12270
  0.00	1	0	F	227387	              Thermodesulfobiaceae
  0.00	1	1	G	227388	                Thermodesulfobium
  0.06	40	0	P	201174	        Actinobacteria
  0.06	39	2	C	1760	          Actinobacteria
  0.01	9	0	O	85009	            Propionibacteriales
  0.01	9	0	F	31957	              Propionibacteriaceae
  0.01	9	0	G	1912216	                Cutibacterium
  0.01	9	8	S	1747	                  Cutibacterium acnes
  0.00	1	0	-	1905725	                    Cutibacterium acnes subsp. defendens
  0.00	1	1	-	1091045	                      Cutibacterium acnes subsp. defendens ATCC 11828
  0.01	8	0	O	2037	            Actinomycetales
  0.01	8	0	F	2049	              Actinomycetaceae
  0.01	6	1	G	1654	                Actinomyces
  0.00	1	1	S	52774	                  Actinomyces slackii
  0.00	1	1	S	544580	                  Actinomyces oris
  0.00	1	0	S	649739	                  Actinomyces sp. oral taxon 848
  0.00	1	1	-	649743	                    Actinomyces sp. oral taxon 848 str. F0332
  0.00	1	0	S	706438	                  Actinomyces sp. oral taxon 171
  0.00	1	1	-	706439	                    Actinomyces sp. oral taxon 171 str. F0337
  0.00	1	1	S	1960083	                  Actinomyces gaoshouyii
  0.00	2	0	G	2529408	                Schaalia
  0.00	2	2	S	1660	                  Schaalia odontolytica
  0.01	7	0	O	85006	            Micrococcales
  0.01	7	0	F	1268	              Micrococcaceae
  0.01	6	0	G	32207	                Rothia
  0.01	6	4	S	43675	                  Rothia mucilaginosa
  0.00	2	2	-	680646	                    Rothia mucilaginosa DY-18
  0.00	1	0	G	1160973	                Auritidibacter
  0.00	1	1	S	2170745	                  Auritidibacter sp. NML130574
  0.01	7	0	O	85007	            Corynebacteriales
  0.01	7	0	F	1653	              Corynebacteriaceae
  0.01	7	1	G	1716	                Corynebacterium
  0.00	1	1	S	1717	                  Corynebacterium diphtheriae
  0.00	1	1	S	146827	                  Corynebacterium simulans
  0.00	1	0	S	161879	                  Corynebacterium kroppenstedtii
  0.00	1	1	-	645127	                    Corynebacterium kroppenstedtii DSM 44385
  0.00	1	0	S	169292	                  Corynebacterium aurimucosum
  0.00	1	1	-	548476	                    Corynebacterium aurimucosum ATCC 700975
  0.00	1	0	S	258224	                  Corynebacterium resistens
  0.00	1	1	-	662755	                    Corynebacterium resistens DSM 45100
  0.00	1	1	S	1050174	                  Corynebacterium epidermidicanis
  0.01	4	0	O	85011	            Streptomycetales
  0.01	4	0	F	2062	              Streptomycetaceae
  0.01	4	0	G	1883	                Streptomyces
  0.00	1	1	S	45398	                  Streptomyces griseoviridis
  0.00	1	1	S	54571	                  Streptomyces venezuelae
  0.00	1	1	S	67267	                  Streptomyces alboflavus
  0.00	1	1	S	92644	                  Streptomyces malaysiensis
  0.00	1	0	O	85004	            Bifidobacteriales
  0.00	1	0	F	31953	              Bifidobacteriaceae
  0.00	1	0	G	1678	                Bifidobacterium
  0.00	1	1	S	28025	                  Bifidobacterium animalis
  0.00	1	0	O	85008	            Micromonosporales
  0.00	1	0	F	28056	              Micromonosporaceae
  0.00	1	0	G	1865	                Actinoplanes
  0.00	1	0	S	196914	                  Actinoplanes friuliensis
  0.00	1	1	-	1246995	                    Actinoplanes friuliensis DSM 7358
  0.00	1	0	C	84998	          Coriobacteriia
  0.00	1	0	O	84999	            Coriobacteriales
  0.00	1	0	F	1643824	              Atopobiaceae
  0.00	1	0	G	1380	                Atopobium
  0.00	1	0	S	1382	                  Atopobium parvulum
  0.00	1	1	-	521095	                    Atopobium parvulum DSM 20469
  0.00	2	0	P	544448	        Tenericutes
  0.00	2	0	C	31969	          Mollicutes
  0.00	2	0	O	2085	            Mycoplasmatales
  0.00	2	0	F	2092	              Mycoplasmataceae
  0.00	2	0	G	2093	                Mycoplasma
  0.00	1	1	S	2113	                  Mycoplasma californicum
  0.00	1	1	S	538219	                  Mycoplasma sp. 2F1A
  0.00	1	0	P	1297	        Deinococcus-Thermus
  0.00	1	0	C	188787	          Deinococci
  0.00	1	0	O	118964	            Deinococcales
  0.00	1	0	F	183710	              Deinococcaceae
  0.00	1	0	G	1298	                Deinococcus
  0.00	1	0	S	310783	                  Deinococcus deserti
  0.00	1	1	-	546414	                    Deinococcus deserti VCD115
  0.00	1	0	-	1798711	        Cyanobacteria/Melainabacteria group
  0.00	1	0	P	1117	          Cyanobacteria
  0.00	1	0	-	1301283	            Oscillatoriophycideae
  0.00	1	0	O	1150	              Oscillatoriales
  0.00	1	0	F	1892254	                Oscillatoriaceae
  0.00	1	0	G	1158	                  Oscillatoria
  0.00	1	0	S	482564	                    Oscillatoria nigro-viridis
  0.00	1	1	-	179408	                      Oscillatoria nigro-viridis PCC 7112
  0.06	38	0	-	1783270	      FCB group
  0.06	38	0	-	68336	        Bacteroidetes/Chlorobi group
  0.06	37	0	P	976	          Bacteroidetes
  0.05	33	0	C	200643	            Bacteroidia
  0.05	33	0	O	171549	              Bacteroidales
  0.04	28	0	F	171552	                Prevotellaceae
  0.04	28	1	G	838	                  Prevotella
  0.02	12	12	S	28132	                    Prevotella melaninogenica
  0.01	7	3	S	28129	                    Prevotella denticola
  0.01	4	4	-	767031	                      Prevotella denticola F0289
  0.01	4	4	S	1177574	                    Prevotella jejuni
  0.00	2	2	S	28131	                    Prevotella intermedia
  0.00	1	1	S	28135	                    Prevotella oris
  0.00	1	0	S	589436	                    Prevotella fusca
  0.00	1	1	-	1236517	                      Prevotella fusca JCM 17724
  0.01	4	0	F	171551	                Porphyromonadaceae
  0.01	4	0	G	836	                  Porphyromonas
  0.01	4	4	S	837	                    Porphyromonas gingivalis
  0.00	1	0	F	2005525	                Tannerellaceae
  0.00	1	0	G	195950	                  Tannerella
  0.00	1	1	S	28112	                    Tannerella forsythia
  0.00	2	0	C	768503	            Cytophagia
  0.00	2	0	O	768507	              Cytophagales
  0.00	1	0	F	89373	                Cytophagaceae
  0.00	1	0	G	107	                  Spirosoma
  0.00	1	1	S	1211326	                    Spirosoma aerolatum
  0.00	1	0	F	1937968	                Bernardetiaceae
  0.00	1	0	G	1937972	                  Bernardetia
  0.00	1	0	S	999	                    Bernardetia litoralis
  0.00	1	1	-	880071	                      Bernardetia litoralis DSM 6794
  0.00	1	0	C	117743	            Flavobacteriia
  0.00	1	0	O	200644	              Flavobacteriales
  0.00	1	0	F	49546	                Flavobacteriaceae
  0.00	1	0	G	237	                  Flavobacterium
  0.00	1	1	S	996	                    Flavobacterium columnare
  0.00	1	0	C	117747	            Sphingobacteriia
  0.00	1	0	O	200666	              Sphingobacteriales
  0.00	1	0	F	84566	                Sphingobacteriaceae
  0.00	1	0	G	84567	                  Pedobacter
  0.00	1	1	S	188932	                    Pedobacter cryoconitis
  0.00	1	0	P	1090	          Chlorobi
  0.00	1	0	C	191410	            Chlorobia
  0.00	1	0	O	191411	              Chlorobiales
  0.00	1	0	F	191412	                Chlorobiaceae
  0.00	1	0	-	274493	                  Chlorobium/Pelodictyon group
  0.00	1	0	G	1099	                    Pelodictyon
  0.00	1	0	S	34090	                      Pelodictyon phaeoclathratiforme
  0.00	1	1	-	324925	                        Pelodictyon phaeoclathratiforme BU-1
  0.00	2	0	P	32066	      Fusobacteria
  0.00	2	0	C	203490	        Fusobacteriia
  0.00	2	0	O	203491	          Fusobacteriales
  0.00	2	0	F	1129771	            Leptotrichiaceae
  0.00	1	0	G	32067	              Leptotrichia
  0.00	1	1	S	554406	                Leptotrichia hongkongensis
  0.00	1	0	G	168808	              Sneathia
  0.00	1	1	S	187101	                Sneathia amnii
  0.00	2	0	P	200783	      Aquificae
  0.00	2	0	C	187857	        Aquificae
  0.00	2	0	O	32069	          Aquificales
  0.00	2	0	-	90150	            Aquificales genera incertae sedis
  0.00	2	0	G	412592	              Thermosulfidibacter
  0.00	2	0	S	412593	                Thermosulfidibacter takaii
  0.00	2	2	-	1298851	                  Thermosulfidibacter takaii ABI70S6
  0.00	2	0	-	1783257	      PVC group
  0.00	1	0	P	74201	        Verrucomicrobia
  0.00	1	0	C	414999	          Opitutae
  0.00	1	0	O	415000	            Opitutales
  0.00	1	0	F	134623	              Opitutaceae
  0.00	1	0	G	2576890	                Nibricoccus
  0.00	1	1	S	2576891	                  Nibricoccus aquaticus
  0.00	1	1	P	203682	        Planctomycetes
  0.00	1	0	-	2323	      unclassified Bacteria
  0.00	1	0	-	1783234	        Bacteria candidate phyla
  0.00	1	0	-	95901	          Candidatus Dependentiae
  0.00	1	0	C	2497643	            Candidatus Babeliae
  0.00	1	0	O	2497644	              Candidatus Babeliales
  0.00	1	0	F	2497645	                Candidatus Babeliaceae
  0.00	1	0	G	1551504	                  Candidatus Babela
  0.00	1	1	S	673862	                    Candidatus Babela massiliensis
  0.00	1	0	P	200918	      Thermotogae
  0.00	1	0	C	188708	        Thermotogae
  0.00	1	0	O	2419	          Thermotogales
  0.00	1	0	F	1643950	            Fervidobacteriaceae
  0.00	1	0	G	2422	              Fervidobacterium
  0.00	1	0	S	93466	                Fervidobacterium pennivorans
  0.00	1	1	-	771875	                  Fervidobacterium pennivorans DSM 9078
  0.00	1	0	P	200930	      Deferribacteres
  0.00	1	0	C	68337	        Deferribacteres
  0.00	1	0	O	191393	          Deferribacterales
  0.00	1	0	F	191394	            Deferribacteraceae
  0.00	1	0	G	2351	              Flexistipes
  0.00	1	0	S	2352	                Flexistipes sinusarabici
  0.00	1	1	-	717231	                  Flexistipes sinusarabici DSM 4947
  0.00	1	0	D	2157	    Archaea
  0.00	1	0	P	28890	      Euryarchaeota
  0.00	1	0	-	2290931	        Stenosarchaea group
  0.00	1	0	C	183963	          Halobacteria
  0.00	1	0	O	1644060	            Natrialbales
  0.00	1	0	F	1644061	              Natrialbaceae
  0.00	1	0	G	332951	                Halovivax
  0.00	1	0	S	387341	                  Halovivax ruber
  0.00	1	1	-	797302	                    Halovivax ruber XH-70
  0.01	9	0	D	10239	  Viruses
  0.01	5	0	F	10508	    Adenoviridae
  0.01	5	0	G	10509	      Mastadenovirus
  0.01	5	2	S	108098	        Human mastadenovirus B
  0.00	3	0	-	565302	          Human adenovirus B1
  0.00	3	3	-	10519	            Human adenovirus 7
  0.00	2	0	O	28883	    Caudovirales
  0.00	2	0	F	10662	      Myoviridae
  0.00	1	0	-	857479	        Peduovirinae
  0.00	1	0	G	1196844	          Hpunavirus
  0.00	1	1	S	10690	            Haemophilus virus HP1
  0.00	1	0	G	2560131	        Ficleduovirus
  0.00	1	0	S	2560473	          Flavobacterium virus FCL2
  0.00	1	1	-	908819	            Flavobacterium phage FCL-2
  0.00	2	0	O	2169561	    Ortervirales
  0.00	2	0	F	11632	      Retroviridae
  0.00	2	0	-	35276	        unclassified Retroviridae
  0.00	2	0	-	206037	          Human endogenous retroviruses
  0.00	2	0	S	45617	            Human endogenous retrovirus K
  0.00	2	2	-	166122	              Human endogenous retrovirus K113
