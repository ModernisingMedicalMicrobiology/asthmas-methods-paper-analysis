 13.75	3647	3647	U	0	unclassified
 86.25	22878	9	-	1	root
 86.21	22866	0	-	131567	  cellular organisms
 63.95	16964	3	D	2759	    Eukaryota
 63.94	16961	0	-	33154	      Opisthokonta
 63.94	16959	0	K	33208	        Metazoa
 63.94	16959	0	-	6072	          Eumetazoa
 63.94	16959	0	-	33213	            Bilateria
 63.94	16959	0	-	33511	              Deuterostomia
 63.94	16959	0	P	7711	                Chordata
 63.94	16959	0	-	89593	                  Craniata
 63.94	16959	0	-	7742	                    Vertebrata
 63.94	16959	0	-	7776	                      Gnathostomata
 63.94	16959	0	-	117570	                        Teleostomi
 63.94	16959	0	-	117571	                          Euteleostomi
 63.94	16959	0	-	8287	                            Sarcopterygii
 63.94	16959	0	-	1338369	                              Dipnotetrapodomorpha
 63.94	16959	0	-	32523	                                Tetrapoda
 63.94	16959	0	-	32524	                                  Amniota
 63.94	16959	0	C	40674	                                    Mammalia
 63.94	16959	0	-	32525	                                      Theria
 63.94	16959	0	-	9347	                                        Eutheria
 63.94	16959	0	-	1437010	                                          Boreoeutheria
 63.94	16959	0	-	314146	                                            Euarchontoglires
 63.94	16959	0	O	9443	                                              Primates
 63.94	16959	0	-	376913	                                                Haplorrhini
 63.94	16959	0	-	314293	                                                  Simiiformes
 63.94	16959	0	-	9526	                                                    Catarrhini
 63.94	16959	0	-	314295	                                                      Hominoidea
 63.94	16959	0	F	9604	                                                        Hominidae
 63.94	16959	0	-	207598	                                                          Homininae
 63.94	16959	0	G	9605	                                                            Homo
 63.94	16959	16959	S	9606	                                                              Homo sapiens
  0.01	2	0	K	4751	        Fungi
  0.01	2	0	-	451864	          Dikarya
  0.01	2	0	P	4890	            Ascomycota
  0.01	2	0	-	716545	              saccharomyceta
  0.01	2	0	-	147538	                Pezizomycotina
  0.01	2	0	-	716546	                  leotiomyceta
  0.01	2	0	-	715989	                    sordariomyceta
  0.01	2	0	C	147550	                      Sordariomycetes
  0.00	1	0	-	222543	                        Hypocreomycetidae
  0.00	1	0	O	5125	                          Hypocreales
  0.00	1	0	F	110618	                            Nectriaceae
  0.00	1	0	G	5506	                              Fusarium
  0.00	1	0	S	171627	                                Fusarium fujikuroi species complex
  0.00	1	0	S	5127	                                  Fusarium fujikuroi
  0.00	1	1	-	1279085	                                    Fusarium fujikuroi IMI 58289
  0.00	1	0	-	222544	                        Sordariomycetidae
  0.00	1	0	O	5139	                          Sordariales
  0.00	1	0	F	35718	                            Chaetomiaceae
  0.00	1	0	G	35719	                              Thielavia
  0.00	1	0	S	35720	                                Thielavia terrestris
  0.00	1	1	-	578455	                                  Thielavia terrestris NRRL 8126
 22.25	5902	4	D	2	    Bacteria
 21.72	5761	3	P	1224	      Proteobacteria
 21.66	5745	2	C	1236	        Gammaproteobacteria
 21.56	5720	0	O	135625	          Pasteurellales
 21.56	5720	15	F	712	            Pasteurellaceae
 21.49	5700	720	G	724	              Haemophilus
 18.69	4957	4768	S	727	                Haemophilus influenzae
  0.21	56	56	-	281310	                  Haemophilus influenzae 86-028NP
  0.16	42	42	-	1232659	                  Haemophilus influenzae 2019
  0.07	18	18	-	262728	                  Haemophilus influenzae R2866
  0.07	18	18	-	1295140	                  Haemophilus influenzae CGSHiCZ412602
  0.05	14	14	-	1334187	                  Haemophilus influenzae KR494
  0.05	12	12	-	262727	                  Haemophilus influenzae R2846
  0.04	10	10	-	71421	                  Haemophilus influenzae Rd KW20
  0.03	8	8	-	866630	                  Haemophilus influenzae F3031
  0.03	7	7	-	862964	                  Haemophilus influenzae 10810
  0.01	2	2	-	374930	                  Haemophilus influenzae PittEE
  0.01	2	2	-	935897	                  Haemophilus influenzae F3047
  0.03	7	7	S	726	                Haemophilus haemolyticus
  0.03	7	6	S	729	                Haemophilus parainfluenzae
  0.00	1	1	-	862965	                  Haemophilus parainfluenzae T3T1
  0.02	6	6	S	197575	                Haemophilus aegyptius
  0.01	2	2	S	712310	                Haemophilus sp. oral taxon 036
  0.00	1	1	S	730	                [Haemophilus] ducreyi
  0.01	2	1	G	416916	              Aggregatibacter
  0.00	1	1	S	732	                Aggregatibacter aphrophilus
  0.00	1	0	G	745	              Pasteurella
  0.00	1	0	S	747	                Pasteurella multocida
  0.00	1	1	-	123812	                  Pasteurella multocida subsp. gallicida
  0.00	1	0	G	75984	              Mannheimia
  0.00	1	0	S	85404	                Mannheimia varigena
  0.00	1	1	-	1433287	                  Mannheimia varigena USDA-ARS-USMARC-1296
  0.00	1	0	G	2094023	              Glaesserella
  0.00	1	0	S	738	                Glaesserella parasuis
  0.00	1	1	-	557723	                  Glaesserella parasuis SH0165
  0.03	8	0	O	72274	          Pseudomonadales
  0.03	7	0	F	468	            Moraxellaceae
  0.02	6	0	G	475	              Moraxella
  0.02	6	6	S	480	                Moraxella catarrhalis
  0.00	1	0	G	469	              Acinetobacter
  0.00	1	0	S	909768	                Acinetobacter calcoaceticus/baumannii complex
  0.00	1	1	S	471	                  Acinetobacter calcoaceticus
  0.00	1	0	F	135621	            Pseudomonadaceae
  0.00	1	0	G	286	              Pseudomonas
  0.00	1	1	S	2479392	                Pseudomonas sp. LTJR-52
  0.03	8	0	O	91347	          Enterobacterales
  0.02	6	1	F	543	            Enterobacteriaceae
  0.02	5	0	G	590	              Salmonella
  0.02	5	1	S	28901	                Salmonella enterica
  0.02	4	1	-	59201	                  Salmonella enterica subsp. enterica
  0.00	1	0	-	119912	                    Salmonella enterica subsp. enterica serovar Choleraesuis
  0.00	1	1	-	938142	                      Salmonella enterica subsp. enterica serovar Choleraesuis str. ATCC 10708
  0.00	1	1	-	149539	                    Salmonella enterica subsp. enterica serovar Enteritidis
  0.00	1	1	-	2579247	                    Salmonella enterica subsp. enterica serovar Rough O:-:-
  0.00	1	0	F	1903410	            Pectobacteriaceae
  0.00	1	0	G	84565	              Sodalis
  0.00	1	0	S	1486991	                Candidatus Sodalis pierantonius
  0.00	1	1	-	2342	                  Candidatus Sodalis pierantonius str. SOPE
  0.00	1	0	F	1903411	            Yersiniaceae
  0.00	1	0	G	613	              Serratia
  0.00	1	1	S	671990	                Serratia sp. FGI94
  0.01	2	0	O	118969	          Legionellales
  0.01	2	0	F	444	            Legionellaceae
  0.00	1	0	G	445	              Legionella
  0.00	1	1	S	456	                Legionella jordanis
  0.00	1	0	G	465	              Tatlockia
  0.00	1	1	S	451	                Tatlockia micdadei
  0.00	1	0	-	118884	          unclassified Gammaproteobacteria
  0.00	1	0	G	745410	            Gallaecimonas
  0.00	1	1	S	2291597	              Gallaecimonas mangrovi
  0.00	1	0	O	135619	          Oceanospirillales
  0.00	1	0	F	28256	            Halomonadaceae
  0.00	1	0	G	2745	              Halomonas
  0.00	1	1	S	272774	                Halomonas alkaliphila
  0.00	1	0	O	135622	          Alteromonadales
  0.00	1	0	F	267888	            Pseudoalteromonadaceae
  0.00	1	0	G	53246	              Pseudoalteromonas
  0.00	1	1	S	43658	                Pseudoalteromonas rubra
  0.00	1	0	O	135623	          Vibrionales
  0.00	1	0	F	641	            Vibrionaceae
  0.00	1	0	G	657	              Photobacterium
  0.00	1	0	S	38293	                Photobacterium damselae
  0.00	1	1	-	38294	                  Photobacterium damselae subsp. piscicida
  0.00	1	0	O	1706369	          Cellvibrionales
  0.00	1	0	F	1706375	            Spongiibacteraceae
  0.00	1	0	G	1084558	              Oceanicoccus
  0.00	1	1	S	716816	                Oceanicoccus sagamiensis
  0.03	8	0	C	28216	        Betaproteobacteria
  0.02	4	1	O	80840	          Burkholderiales
  0.01	2	1	F	80864	            Comamonadaceae
  0.00	1	0	G	34072	              Variovorax
  0.00	1	1	S	2126319	                Variovorax sp. PMC12
  0.00	1	0	F	75682	            Oxalobacteraceae
  0.00	1	0	G	963	              Herbaspirillum
  0.00	1	1	S	863372	                Herbaspirillum huttiense
  0.01	3	0	O	206351	          Neisseriales
  0.01	2	1	F	481	            Neisseriaceae
  0.00	1	0	G	71	              Simonsiella
  0.00	1	0	S	72	                Simonsiella muelleri
  0.00	1	1	-	641147	                  Simonsiella muelleri ATCC 29453
  0.00	1	0	F	1499392	            Chromobacteriaceae
  0.00	1	0	G	568394	              Pseudogulbenkiania
  0.00	1	1	S	748280	                Pseudogulbenkiania sp. NH8B
  0.00	1	0	O	206389	          Rhodocyclales
  0.00	1	1	F	2008794	            Zoogloeaceae
  0.01	3	0	-	68525	        delta/epsilon subdivisions
  0.01	3	0	C	29547	          Epsilonproteobacteria
  0.01	3	0	O	213849	            Campylobacterales
  0.01	3	0	F	72294	              Campylobacteraceae
  0.01	3	0	G	194	                Campylobacter
  0.01	2	1	S	199	                  Campylobacter concisus
  0.00	1	1	-	360104	                    Campylobacter concisus 13826
  0.00	1	1	S	204	                  Campylobacter showae
  0.01	2	0	C	28211	        Alphaproteobacteria
  0.00	1	0	O	766	          Rickettsiales
  0.00	1	0	-	210592	            unclassified Rickettsiales
  0.00	1	0	-	1699067	              unclassified Rickettsiales (miscellaneous)
  0.00	1	1	S	2486578	                Rickettsiales endosymbiont of Stachyamoeba lipophora
  0.00	1	0	O	204457	          Sphingomonadales
  0.00	1	0	F	335929	            Erythrobacteraceae
  0.00	1	0	G	1041	              Erythrobacter
  0.00	1	1	S	39960	                Erythrobacter litoralis
  0.25	67	0	-	1783270	      FCB group
  0.25	67	0	-	68336	        Bacteroidetes/Chlorobi group
  0.25	67	0	P	976	          Bacteroidetes
  0.25	65	0	C	200643	            Bacteroidia
  0.25	65	0	O	171549	              Bacteroidales
  0.22	59	0	F	171552	                Prevotellaceae
  0.22	59	1	G	838	                  Prevotella
  0.09	23	23	S	1177574	                    Prevotella jejuni
  0.06	17	17	S	28135	                    Prevotella oris
  0.04	11	10	S	28131	                    Prevotella intermedia
  0.00	1	1	-	1122984	                      Prevotella intermedia ATCC 25611 = DSM 20706
  0.02	4	4	S	28132	                    Prevotella melaninogenica
  0.01	3	3	S	76123	                    Prevotella enoeca
  0.02	6	0	F	2005525	                Tannerellaceae
  0.02	6	0	G	195950	                  Tannerella
  0.02	6	1	S	28112	                    Tannerella forsythia
  0.01	3	3	-	1307832	                      Tannerella forsythia 3313
  0.01	2	2	-	1307833	                      Tannerella forsythia KS16
  0.01	2	0	C	117743	            Flavobacteriia
  0.01	2	0	O	200644	              Flavobacteriales
  0.01	2	0	F	49546	                Flavobacteriaceae
  0.01	2	0	G	1016	                  Capnocytophaga
  0.00	1	1	S	1017	                    Capnocytophaga gingivalis
  0.00	1	1	S	45243	                    Capnocytophaga haemolytica
  0.24	64	0	-	1783272	      Terrabacteria group
  0.15	40	0	P	1239	        Firmicutes
  0.07	19	0	C	909932	          Negativicutes
  0.07	18	0	O	1843489	            Veillonellales
  0.07	18	0	F	31977	              Veillonellaceae
  0.06	17	0	G	29465	                Veillonella
  0.06	15	15	S	39778	                  Veillonella dispar
  0.01	2	2	S	29466	                  Veillonella parvula
  0.00	1	0	G	39948	                Dialister
  0.00	1	1	S	39950	                  Dialister pneumosintes
  0.00	1	0	O	909929	            Selenomonadales
  0.00	1	0	F	1843491	              Selenomonadaceae
  0.00	1	0	G	970	                Selenomonas
  0.00	1	1	S	713030	                  Selenomonas sp. oral taxon 136
  0.07	18	0	C	91061	          Bacilli
  0.05	13	0	O	186826	            Lactobacillales
  0.04	11	0	F	1300	              Streptococcaceae
  0.04	11	2	G	1301	                Streptococcus
  0.01	3	1	S	1318	                  Streptococcus parasanguinis
  0.01	2	2	-	1114965	                    Streptococcus parasanguinis FW213
  0.01	2	1	S	1304	                  Streptococcus salivarius
  0.00	1	1	-	1048332	                    Streptococcus salivarius CCHSS3
  0.00	1	0	S	1307	                  Streptococcus suis
  0.00	1	1	-	1004952	                    Streptococcus suis D12
  0.00	1	1	S	1311	                  Streptococcus agalactiae
  0.00	1	1	S	28037	                  Streptococcus mitis
  0.00	1	1	S	78535	                  Streptococcus viridans
  0.01	2	0	F	81850	              Leuconostocaceae
  0.00	1	0	G	1243	                Leuconostoc
  0.00	1	1	S	1246	                  Leuconostoc lactis
  0.00	1	0	G	46255	                Weissella
  0.00	1	0	S	165096	                  Weissella koreensis
  0.00	1	1	-	1045854	                    Weissella koreensis KACC 15510
  0.02	5	0	O	1385	            Bacillales
  0.02	5	0	F	186817	              Bacillaceae
  0.01	3	0	G	1386	                Bacillus
  0.00	1	0	S	1398	                  Bacillus coagulans
  0.00	1	1	-	345219	                    Bacillus coagulans 36D1
  0.00	1	0	S	86661	                  Bacillus cereus group
  0.00	1	1	S	1396	                    Bacillus cereus
  0.00	1	1	S	1742359	                  Bacillus sp. FJAT-25496
  0.01	2	0	G	400634	                Lysinibacillus
  0.00	1	1	S	28031	                  Lysinibacillus fusiformis
  0.00	1	1	S	2070463	                  Lysinibacillus sp. SGAir0095
  0.01	3	0	C	186801	          Clostridia
  0.01	2	0	O	186802	            Clostridiales
  0.01	2	0	F	186803	              Lachnospiraceae
  0.01	2	0	G	1506553	                Lachnoclostridium
  0.00	1	0	S	84030	                  [Clostridium] saccharolyticum
  0.00	1	1	-	610130	                    [Clostridium] saccharolyticum WM1
  0.00	1	1	S	1871021	                  Lachnoclostridium phocaeense
  0.00	1	0	O	68295	            Thermoanaerobacterales
  0.00	1	0	F	543371	              Thermoanaerobacterales Family III. Incertae Sedis
  0.00	1	0	G	28895	                Thermoanaerobacterium
  0.00	1	1	S	1517	                  Thermoanaerobacterium thermosaccharolyticum
  0.09	23	0	P	201174	        Actinobacteria
  0.06	15	0	C	1760	          Actinobacteria
  0.02	5	0	O	85006	            Micrococcales
  0.02	4	0	F	1268	              Micrococcaceae
  0.02	4	0	G	32207	                Rothia
  0.02	4	3	S	43675	                  Rothia mucilaginosa
  0.00	1	1	-	680646	                    Rothia mucilaginosa DY-18
  0.00	1	0	F	85020	              Dermabacteraceae
  0.00	1	0	G	43668	                Brachybacterium
  0.00	1	1	S	2017485	                  Brachybacterium sp. VR2415
  0.02	4	0	O	85009	            Propionibacteriales
  0.02	4	0	F	31957	              Propionibacteriaceae
  0.01	2	0	G	1743	                Propionibacterium
  0.01	2	2	S	671223	                  Propionibacterium sp. oral taxon 193
  0.01	2	0	G	1912216	                Cutibacterium
  0.01	2	2	S	1747	                  Cutibacterium acnes
  0.01	3	0	O	2037	            Actinomycetales
  0.01	3	0	F	2049	              Actinomycetaceae
  0.01	2	0	G	1654	                Actinomyces
  0.01	2	2	S	1852377	                  Actinomyces pacaensis
  0.00	1	0	G	2529408	                Schaalia
  0.00	1	1	S	1660	                  Schaalia odontolytica
  0.01	2	0	O	85007	            Corynebacteriales
  0.01	2	0	F	1762	              Mycobacteriaceae
  0.01	2	0	G	1763	                Mycobacterium
  0.00	1	0	S	29311	                  Mycobacterium haemophilum
  0.00	1	1	-	1202450	                    Mycobacterium haemophilum DSM 44634
  0.00	1	0	S	120793	                  Mycobacterium avium complex (MAC)
  0.00	1	0	S	1767	                    Mycobacterium intracellulare
  0.00	1	0	-	35617	                      Mycobacterium intracellulare subsp. intracellulare
  0.00	1	1	-	1232724	                        Mycobacterium intracellulare subsp. intracellulare MTCC 9506
  0.00	1	0	O	2039638	            Candidatus Nanopelagicales
  0.00	1	0	F	2162846	              Candidatus Nanopelagicaceae
  0.00	1	0	G	2039639	                Candidatus Nanopelagicus
  0.00	1	1	S	1884915	                  Candidatus Nanopelagicus hibericus
  0.03	8	0	C	84998	          Coriobacteriia
  0.03	8	0	O	84999	            Coriobacteriales
  0.03	8	0	F	1643824	              Atopobiaceae
  0.03	8	0	G	1380	                Atopobium
  0.03	8	0	S	1382	                  Atopobium parvulum
  0.03	8	8	-	521095	                    Atopobium parvulum DSM 20469
  0.00	1	0	-	1798711	        Cyanobacteria/Melainabacteria group
  0.00	1	0	P	1117	          Cyanobacteria
  0.00	1	0	-	1301283	            Oscillatoriophycideae
  0.00	1	0	O	1150	              Oscillatoriales
  0.00	1	0	F	1892254	                Oscillatoriaceae
  0.00	1	0	G	1155738	                  Moorea
  0.00	1	0	S	1155739	                    Moorea producens
  0.00	1	1	-	1458985	                      Moorea producens PAL-8-15-08-1
  0.01	2	0	P	32066	      Fusobacteria
  0.01	2	0	C	203490	        Fusobacteriia
  0.01	2	0	O	203491	          Fusobacteriales
  0.01	2	0	F	203492	            Fusobacteriaceae
  0.01	2	1	G	848	              Fusobacterium
  0.00	1	0	S	849	                Fusobacterium gonidiaformans
  0.00	1	1	-	469615	                  Fusobacterium gonidiaformans ATCC 25563
  0.01	2	0	P	203691	      Spirochaetes
  0.01	2	0	C	203692	        Spirochaetia
  0.01	2	0	O	136	          Spirochaetales
  0.01	2	0	F	137	            Spirochaetaceae
  0.01	2	0	G	157	              Treponema
  0.00	1	0	S	81028	                Treponema brennaborense
  0.00	1	1	-	906968	                  Treponema brennaborense DSM 12168
  0.00	1	1	S	1539298	                Treponema sp. OMZ 838
  0.00	1	0	P	40117	      Nitrospirae
  0.00	1	0	C	203693	        Nitrospira
  0.00	1	0	O	189778	          Nitrospirales
  0.00	1	0	F	189779	            Nitrospiraceae
  0.00	1	0	G	1234	              Nitrospira
  0.00	1	1	S	1715989	                Candidatus Nitrospira inopinata
  0.00	1	0	-	1783257	      PVC group
  0.00	1	0	P	203682	        Planctomycetes
  0.00	1	0	-	473814	          unclassified Planctomycetes
  0.00	1	1	S	2528029	            Planctomycetes bacterium Pan153
  0.01	3	0	D	10239	  Viruses
  0.01	2	0	O	28883	    Caudovirales
  0.00	1	0	F	10662	      Myoviridae
  0.00	1	0	-	196896	        unclassified Myoviridae
  0.00	1	1	S	1815521	          Synechococcus phage S-WAM1
  0.00	1	0	F	10699	      Siphoviridae
  0.00	1	0	G	1921122	        Nonanavirus
  0.00	1	0	-	2315205	          unclassified Nonanavirus
  0.00	1	1	S	1969841	            Proteus phage VB_PmiS-Isfahan
  0.00	1	0	O	2169561	    Ortervirales
  0.00	1	0	F	11632	      Retroviridae
  0.00	1	0	-	35276	        unclassified Retroviridae
  0.00	1	0	-	206037	          Human endogenous retroviruses
  0.00	1	0	S	45617	            Human endogenous retrovirus K
  0.00	1	1	-	166122	              Human endogenous retrovirus K113
