#!/bin/env python3
import pandas as pd

df=pd.read_csv('seqkit_data.tsv' ,sep='\t')
meta=pd.read_csv('meta.tsv',names=['barcode','run','Sample ID','PID','info'] ,sep='\t')

df=df.merge(meta, left_on=['run','sample'], right_on=['run','barcode'],how='left')
df.to_csv('seqkit_data_meta.tsv',index=False,sep='\t')
print(df)
