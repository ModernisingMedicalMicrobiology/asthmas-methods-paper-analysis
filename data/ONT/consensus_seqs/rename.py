#!/usr/bin/env python3
from Bio import SeqIO
import sys, os
import pandas as pd

files=os.listdir('clairConsensuSeqs')
files=[f for f in files if f.endswith('fasta')]

seqs=[]
stats=[]
for f in files:
    name=f.split('.')[0]
    for seq in SeqIO.parse(open('clairConsensuSeqs/{}'.format(f),'rt'),'fasta'):
        seq.id=name
        seqs.append(seq)
        d={}
        d['sample']=name
        d['Ns']=seq.seq.count('N')
        d['As']=seq.seq.count('A')
        d['Ts']=seq.seq.count('T')
        d['Cs']=seq.seq.count('C')
        d['Gs']=seq.seq.count('G')
        d['length']=len(seq.seq)
        d['bases covered']=d['As'] + d['Ts'] + d['Cs'] +d['Gs']
        d['covered %']=(d['bases covered']/d['length'])*100
        stats.append(d)

df=pd.DataFrame(stats)
print(df)
df.to_csv('map_coverage_stats.csv',index=False)

with open('renamed.fasta','wt') as outf:
    SeqIO.write(seqs,outf, 'fasta')
